function g_11expPixel(img)
    img = double(img);
    subplot(1,2,1);
    imshow(uint8(img));
    img_exp(:,:,:) = exp(img(:,:,:));
    
    subplot(1,2,2);
    imshow(uint8(img_exp));
end
function res = drawTriangle(dim, h)
res = zeros(dim);
st=(dim-h)/2;
for i=1:h
   res(st+i-1, dim/2-i:dim/2+i) = 255;
end

end
function res = drawCircle(dim, r)
c=dim/2;
res=zeros(dim);
inCircle = @(x,y) (x-c)^2+(y-c)^2-r^2;
for i=1:dim
    for j=1:dim
        if inCircle(i,j) < 0
            res(i,j)=255;
        end
        

%       res(i,j)=inCircle(i,j);
    end
end
end
function g_12block(img)
    %sezione dei blocchi
    N = size(img,1)/2;
    M = size(img,2)/2;
    
    %sezione dell'immagine
    block = img(1:N,1:M,:);
    imshow(block);
end
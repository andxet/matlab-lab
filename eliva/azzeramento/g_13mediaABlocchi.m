function g_13mediaABlocchi(img)
    bxr = 128;
    dim = size(img,1)/bxr;
    im2 = img;
    for i=1:bxr
        for j=1:bxr
            im2(dim*(i-1)+1:dim*i,dim*(j-1)+1:dim*j) = mean(mean(im2(dim*(i-1)+1:dim*i,dim*(j-1)+1:dim*j)));
        end
    end
    imshow(im2);
end
function g_cosImage
    f = 0.015;
    A = cos(2*pi*f*[1:256]')*ones(1,256);
    imshow(real(A));
end
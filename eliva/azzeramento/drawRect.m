function ris = drawRect(dim, w,h)
ris=zeros(dim);
cw=(dim-w)/2;
ch=(dim-h)/2;
ris(cw:cw+w, ch:ch+h) = 255;
end
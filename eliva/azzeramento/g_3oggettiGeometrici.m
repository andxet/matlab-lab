function g_3oggettiGeometrici(dim)
    % rettangolo
    ris=zeros(dim);
    w=floor(dim/2);
    h=floor(dim/3);
    cw=floor((dim-w)/2);
    ch=floor((dim-h)/2);
    ris(cw:cw+w, ch:ch+h) = 255;
    subplot(1,3,1);
    imagesc(ris);
    
    %triangolo
    res = zeros(dim);
    st=floor((dim-h)/2);
    for i=1:h
       res(st+i-1, dim/2-i:dim/2+i) = 255;
    end
    subplot(1,3,2);
    imagesc(res);
    
    %cerchio
    c=dim/2;
    r = dim/3;
    cer=zeros(dim);
    inCircle = @(x,y) (x-c)^2+(y-c)^2-r^2;
    for i=1:dim
        for j=1:dim
            if inCircle(i,j) < 0
                cer(i,j)=255;
            end
        end
    end
    subplot(1,3,3);
    imagesc(cer);
end
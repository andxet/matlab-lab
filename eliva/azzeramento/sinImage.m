function result = sinImage(dim, f)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
maxx=dim;
maxy=maxx;
maxz=dim; minz=0;
x=0:maxx;
s=sin(2*pi*f*x/(maxx+1));
fm=s-min(s);
fs=(maxz-minz)*(fm/max(fm))+minz;
w=fs.';
%result=ones(maxy+1,1)*fs;
result=w*ones(maxy+1,1)';
imshow(uint8(result)); %y increases downwar

end
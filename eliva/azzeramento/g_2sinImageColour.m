function g_2sinImageColour
    f1 = 0.015;
    f2 = 0.025;
    f3 = 0.035;
    
    A(1:256,1:256,1) = sin(2*pi*f1*[1:256]')*ones(1,256);
    A(1:256,1:256,2) = sin(2*pi*f2*[1:256]')*ones(1,256);
    A(1:256,1:256,3) = sin(2*pi*f3*[1:256]')*ones(1,256);
    
    imshow(real(A));

end
function g_15maxABlocchi(img)
    bxr = 128;
    dim = size(img,1)/bxr;
    im2 = img;
    for i=1:bxr
        for j=1:bxr
            im2(dim*(i-1)+1:dim*i,dim*(j-1)+1:dim*j) = max(max(im2(dim*(i-1)+1:dim*i,dim*(j-1)+1:dim*j)));
        end
    end
    %confronto con l'immagine originale
    subplot(1,3,1);
    imshow(img);
    subplot(1,3,2);
    imshow(im2);
    subplot(1,3,3);
    imshow(abs(im2-img));
end
function res = addNoise(img)
res = imnoise(img, 'gaussian', 0.000001, 0.05);
end
function g_6imgRGB(img)
    imgR = img(:,:,1);
    imgG = img(:,:,2);
    imgB = img(:,:,3);
    subplot(1,3,1)
    imshow(imgR);
    subplot(1,3,2)
    imshow(imgG);
    subplot(1,3,3)
    imshow(imgB);
end
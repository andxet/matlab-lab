function g_10imgRibalted(img)
    M = size(img,1);
    N = size(img,2);
    
    subplot(1,3,1);
    imshow(img);
    %ribaltato per righe
    img_rib(:,:,:) = img(M:-1:1,:,:);
    subplot(1,3,2);
    imshow(img_rib);
    
    %ribaltato per colonne
    img_rib(:,:,:) = img(:,N:-1:1,:);
    subplot(1,3,3);
    imshow(img_rib);
end
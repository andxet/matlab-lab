function g_9negativo(img)
    neg = double(img);
    max_val = max(max(max(neg(:,:,:))));
    neg(:,:,:) = neg(:,:,:)./max_val*256-1;
    neg(:,:,:) = 256-neg(:,:,:);
    imshow(uint8(neg));
end
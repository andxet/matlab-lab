function g_1d_sobelFilter(img)
    %orizzontale
    ho = [-1,-2,-1;0,0,0;1,2,1];
    subplot(1,3,1);
    imshow(imfilter(img,ho));
    
    %verticale
    hv = [-1,0,1;-2,0,2;-1,0,1];
    subplot(1,3,2);
    imshow(imfilter(img,hv));
    
    %entrambi 
    h = ho+hv;
    subplot(1,3,3);
    imshow(imfilter(img,h));
    
    %fase e modulo???
    
end
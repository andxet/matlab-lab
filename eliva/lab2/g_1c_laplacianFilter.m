function g_1c_laplacianFilter(img)
    %isotropico
    I = [-1,-1,-1;-1,8,-1;-1,-1,-1];
    subplot(1,2,1);
    imshow(imfilter(img, I));
    
    %non isotropico
    NI = [0,-1,0;-1,4,-1;0,-1,0];
    subplot(1,2,2);
    imshow(imfilter(img, NI));
end
function Out=im_ramp_fun(In,r1,r2)
%function Out=im_ramp_fun(In,r1,r2)
In=double(In);


Out=uint8((In-r1)./(r2-r1)*255);
        
    

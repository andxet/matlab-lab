% Questo programma � utilizzabile unicamente per la preparazione
% dell�esame di Elaborazione di Immagini e Visione Artificiale
% del Corso di Laurea Magistrale in Informatica 
% del Dipartimento di Informatica dell�Universit� degli Studi di Torino.
% Ogni altro utilizzo totale o parziale � espressamente vietato, incluse
% la copia digitale o fotostatica e la pubblicazione anche su Internet.

% Performs a point processing of pixel, with a LUT

% Uncomment the LUT of interest and eventually change some constants

clear;

maxlevel = 255;
baselut = [0:maxlevel]/maxlevel;

% NEGATIVE LUT
% lut = uint8((1 - baselut)*maxlevel);

% GAMMA LUT
% gamma = 1.85;
% lut = uint8((baselut.^gamma)*maxlevel);

% LOGARITHMIC LUT
%lut = uint8((log10(1+150*baselut)/log10(max(1+150*baselut)))*maxlevel);

% STEP LUT
% stepdim = 0.11;
% lut = uint8(round(baselut/stepdim)/max(baselut/stepdim)*maxlevel);

% LEVELS EVIDENTIATION LUT
l1 = 52; l2 = 73;
lut = uint8([0:maxlevel]);
lut(1:l1) = 0;
lut(l2:maxlevel+1) = 0;
lut(l1+1:l2-1) = 255;

% BIT LEVEL SLICING, OTHER EFFICIENT WAYS EXISTS!!
% BIT PLANE 8
% lut(1:128) = uint8(0);
% lut(129:256) = uint8(255);

% BIT PLANE 7
% lut(1:64) = uint8(0);
% lut(65:128) = uint8(255);
% lut(129:192) = uint8(0);
% lut(193:256) = uint8(255);

% BIT PLANE 6
% lut(1:32) = uint8(0);
% lut(33:64) = uint8(255);
% lut(65:96) = uint8(0);
% lut(97:128) = uint8(255);
% lut(129:160) = uint8(0);
% lut(161:192) = uint8(255);
% lut(193:224) = uint8(0);
% lut(225:256) = uint8(255);

A = imread('DD1.tif');
imshow(A);
B = intlut(A, lut);
figure; imshow(B);
figure; plot(lut);


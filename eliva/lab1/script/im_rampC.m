function Out=im_rampC(In,r1,r2)
%function Out=im_rampC(In,r1,r2)
In=double(In);
Out=In;
for m=1:size(In,1)
    for n=1:size(In,2)
        if In(m,n)<=r1
            Out(m,n)=0;
        elseif In(m,n) > r2
            Out(m,n)=255;
        else
            Out(m,n)=round((In(m,n)-r1)/(r2-r1)*255);
        end
    end
end
    

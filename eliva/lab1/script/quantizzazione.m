function exp_quant_uni(image,rate,fig)
%function exp_quant_uni(image,rate)
A=pgmread(image);
%if(fig==1)
%    figure;
%end
%imagesc(A)
D=zeros(size(rate));
range=256;
for ind=1:length(rate)
    delta=range/(2^(rate(ind)));
    Aq=round(A./delta).*delta;
    D(ind)=mean((A(:)-Aq(:)).^2);
    if(fig==1)
        %subplot(1,length(rate),ind);
        figure;
        imagesc(Aq);
        axis off;
        axis equal;
        colormap(gray);
        title(['R=',num2str(rate(ind)), ' D=',num2str(D(ind))]);
    end
end
figure;
model=(range^2)./(12.*2.^(2.*rate));
plot(rate,D,'o--r',rate,model,'x--b');
xlabel('rate [bit]');
ylabel('D');
legend('Dati sperimentali','Modello (High rate)');
grid on;


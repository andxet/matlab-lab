function Out=im_ramp(In,r1,r2)
%function Out=im_ramp(In,r1,r2)
range=255;
imdim=size(In);
lut(1:r1-1)=0;
lut(r1:r2)=[0:r2-r1]'.*range./(r2-r1);
lut(r2+1:range+1)=range;
lut=round(lut);
lut=uint8(lut');

% figure;
% plot([0 :range],lut,'o-');
% axis([0 range 0 range]);


if (length(imdim)==2) % grayscale
    In=In(:);
    Out=lut(In+1);
    Out=reshape(Out,imdim(1),imdim(2));
else
    Out=In;
    for d=1:imdim(3)
        bIn=In(:,:,d);
        bIn=bIn(:);
        bOut=lut(bIn+1);
        Out(:,:,d)=reshape(bOut,imdim(1),imdim(2));
    end
end
function g_2eqIstogramma(img)
    M = size(img,1);
    N = size(img,2);

    subplot(1,3,1);
    imshow(img);
    
    subplot(1,3,2);
    imhist(img)
    
    vect = zeros(1,256);
    for i=1:M
        for j=1:N
            vect(img(i,j)) = vect(img(i,j))+1;
        end
    end
    vect = floor(vect./max(vect)*256);
    matr = ones(256);
    
    for i=1:256
        matr(1:vect(i),i) = 0;
    end
    
    matr(:,:) = matr(256:-1:1,:);
    
    subplot(1,3,3);
    imshow(matr);
end
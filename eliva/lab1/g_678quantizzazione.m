%lettura immagine
%riduzione dei livelli
%calcolo errore quadratico
%calcolo distorsione
%grafico con quello atteso
function [D,Da] = g_678quantizzazione
clear all;
X = double(imread('img_test/goldhill.pgm'));

D = zeros(1,8);
Da = zeros(1,8);

for i=1:8
    %quantizzazione
    Xq = (floor(X./(2^(i-1)))).*(2^(i-1));
    %calcolo dell'errore
    Er2 = (X-Xq).^2;
    D(i) = mean(Er2(:));
    Da(i) = ((256.0/2^(8-i+1))^2)/12;
    subplot(2,4,i);
    imshow(uint8(Xq));
end

figure;
hold on;
plot(8:-1:1,D);

plot(8:-1:1, Da,'r');
hold off;

end
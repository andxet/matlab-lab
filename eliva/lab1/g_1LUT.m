function g_1LUT(img)
    subplot(1,2,1);
    imshow(img);
    
    M = size(img,1);
    N = size(img,2);
    img = double(img);
    
    %lut a rampa
%     min = 0;
%     max_val = 255;
%     int_min = 30;
%     int_max = 150;
%     lut = 1:256;
%     
%     lut(min+1:int_min) = 0;
%     lut(int_min+1:int_max) = (lut(int_min+1:int_max)-(int_min+2))/(int_max-int_min)*255;
%     lut(int_max+1:max_val+1) = 255;
    
    %lut a scala
%     gradini = 2;
%     l = 256/gradini;
%     
%     lut = 1:256;
%     lut(:) = floor(lut(:)/l)*l;
    
%     img_lut = img;
%     for I=1:M
%         for J=1:N
%             img_lut(I,J) = img(I,J)/255.*lut(img(I,J));
%         end
%     end
    
    %pseudocolori
    A=255*hsv(256);
    R = A(:,1);
    G = A(:,2);
    B = A(:,3);
    
    for I=1:M
        for J=1:N
            img_lut(I,J,1) = img(I,J)/255.*R(img(I,J));
            img_lut(I,J,2) = img(I,J)/255.*G(img(I,J));
            img_lut(I,J,3) = img(I,J)/255.*B(img(I,J));
        end
    end
    
    subplot(1,2,2);
    imshow(uint8(img_lut));
end
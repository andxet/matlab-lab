function g_3mediaLocale(img)
    %problema del bordo con w grande

    M = size(img,1);
    N = size(img,2);
    w = 9;
    more = ceil(w/2);
    less = floor(w/2);
    res = img;
    for i=more:M-less
        for j=more:N-less
            res(i,j) = mean2(img(i-less:i+less,j-less:j+less));
        end
    end
    imshow(uint8(res));
end
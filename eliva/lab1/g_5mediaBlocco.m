function g_5mediaBlocco(img)
    %problema del bordo se w non divide M o N

    M = size(img,1);
    N = size(img,2);
    w = 5;
    
    res = img;
    for i=1:w:M-w
        for j=1:w:N-w
            res(i:i+w,j:j+w) = mean2(img(i:i+w,j:j+w));
        end
    end
    imshow(uint8(res));
end
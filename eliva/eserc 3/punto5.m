% GLPF filter mask
[X,Y]=meshgrid(1:512,1:512);
D0=32;
D=(X-size(X,2)/2-1).^2+(Y-size(Y,1)/2-1).^2;
H=exp(-D./2./D0);
imagesc(H)
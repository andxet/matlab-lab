function notch
    I = double(imread('../img_test/lena512.pgm'));
    A = 25;
    f0 = 0.25;
    N = sin(f0*2*pi*[1:512]')*ones(1,512);
    img = A+N;
end
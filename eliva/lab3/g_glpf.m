function out = glpf(Inoised)
    M = size(Inoised,1);
    N = size(Inoised,2);
    [X,Y] = meshgrid(1:M,1:N);
    % distanza del punto dal centro
    D = (X-M/2-1).^2 + (Y-N/2-1).^2;
    %Dshift = fftshift(D);
    % calcolo del filtro
    D0 = 50;
    
    H = exp(-D./2./(D0*D0));
    
    subplot(2,2,3);
    imshow(H);
    %Hshift = fftshift(H);
    
    Ifft = fftshift(fft2(Inoised));
    
    img = Ifft.*H;
    out = uint8(real(ifft2(ifftshift(img))));
    
end
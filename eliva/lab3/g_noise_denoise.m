%costruisce un immagine sporcata da un rumore additivo e poi con il 
%filtro glpf cerca di pulire quest'immagine
function noise_denoise
    I = double(imread('../img_test/barbara.pgm'));
    subplot(2,4,1);
    imagesc(I);
    var = 10; %varianza
    N = var.*randn(size(I)); %rumore
    Inoised = I+N; %immagine sporcata
    %%%%
    subplot(2,2,2);
    imagesc(Inoised);
    %%%%
    %applicazione dei filtri mediana e glpf
    img_med = medfilt2(Inoised);
    img_glpf = glpf(Inoised);
    %%%%
    subplot(2,2,3);
    imagesc(img_med);
    subplot(2,2,4);
    imagesc(img_glpf);
    %%%
end
%Si costruiscano semplici immagini di prova costituite da cosinusoidi (per righe e/o colonne) e si 
%rappresenti la loro trasformata di Fourier

function res = fftcos()
    res = ones(512);
    for i=1:512
       res1(1:512,i) = ((cos(0.1 * 2 * pi * i))+1)/2*512; 
       res2(i,1:512) = ((cos(0.1 * 2 * pi * i))+1)/2*512; 
    end
    res = res1+res2;
    subplot(1,2,1);
    imagesc(res);
    subplot(1,2,2);
    imfft = fft2(res);
    imagesc(abs(fftshift(imfft)));
end
function [filt] = GLPF(A)

% Filters an image with a Low pass mask (average)

clear;

% LOW PASS
filter = ones(3);  % filter size 3x3
filter = filter/sum(filter(:));

%A = imread('../goldhill.pgm');
imshow(A);

filt = filter2(filter, double(A), 'same');
figure; imshow(uint8(B));

end
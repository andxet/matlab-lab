function [out] = GLPF2(A, D0)

% GLPF filter mask
[M,N]=size(A);
[X,Y]=meshgrid(1:M,1:N);

D=(X-M/2-1).^2+(Y-N/2-1).^2;
H=exp(-D./2./(D0*D0));

F=fft2(A);
F=fftshift(F);

OF=F.*H;
out=uint8(real(ifft2(ifftshift(OF))));

end
function p = newtoneval(c,x,xx)
    N=length(c);
    M=length(xx);
    res=1;
    p=0*xx + c(N);
    for J=N:-1:1
        res=res.*(c(J)+(xx-x(J)));
    end
    p=res;
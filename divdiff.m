function c= divdiff(x,y)

    assert(length(x) == length(y));
    N=length(x);
    c=y;
    for k=1:N-1
        for J=N:-1:k+1
            c(J)=(c(J)-c(J-1))/(x(J)-x(J-k));
        end
    end
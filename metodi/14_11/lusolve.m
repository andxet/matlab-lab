function x = lusolve( b, B, p )
%risolve il sistema lineare con termine noto b.
%(B,p) sono l'ouput di ludecomp
%ovvero U=triu(B(p,:)) e
%       L = eye(N)+tril(B(p,:)),-1)
%per risolvere A*x=b, risolviamo L*U*x=b(p,:)
% risolvendo prima il sistema tr. inferiore L*y=b(p,:)
% col metodo di sostituzione in avanti
% e poi quello tr. sup U*x=y col metodo di sostituzione all'indietro

%NOTA: non occorre formare esplicitamente le matrici L, U
%per il debug:
% i comandi [B,p]= ludecomp(A); x=lusolve(b,B,p) dovrebbero fornire lo
% stesso risultato di x=A\b

%Aggiungo il pivoting. Se sono pigro scambio le righe delle matrici in
%ingresso. Se sono un programmatore numerico invece...

N=length(B);
if nargin < 3
    p=1:N; %Assume che non sia stato fatto pivoting
end

y=b;

for k=2:N
    j=1:k-1;
    y(k)=b(k)-B(k,j)*y(j);
end
x=y;

for k=N:-1:1
        J=k+1:N;
        x(k)=(y(k)-B(k,J)*x(J)) / B(k,k);
end

% %Debug, forse non tiene conto del pivoting
% U=triu(B);
% L=eye(length(B))+tril(B,-1);
% A = L*U;
% disp(A\b)
% % disp(x)

end


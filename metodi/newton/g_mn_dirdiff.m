%x = vettore delle ordinate dei punti
%y = vettore dei valori dei punti

function [c] = mn_dirdiff(x,y)
 N=length(x);
 c=y;
 for k=1:N-1
     %differenze divise di ordine k
     for j=N:-1:k+1
         %loop all'indietro per non sovrascrivere dati
         %che devo ancora utilizzare
         c(j) = (c(j) - c(j-1))/(x(j) - x(j-k));
     end
 end
end


%usa un'operazione fra vettori
function [res] = mn_dirdiff_opvet(x,y)
    assert(length(x) == length(y));
    x = x';
    y = y';
    len = length(x);
    vet = zeros(len);
    vet(1:len,1) = y;
    for j=2:len
        I = 1:len-j+1;
        vet(I,j) = (vet(I+1,j-1)-vet(I,j-1))./(x(I+j-1)-x(I));
    end
    res = vet(1,:);
end

%usa un solo vettore vet e uno di appoggio temp
function [res] = mn_dirdiff_vet(x,y)
    assert(length(x) == length(y));
    len = size(x,2);
    vet = y;
    temp = y;
    offset = 1;
    res = zeros(len,1);
    while (offset <= len)
        for i=1:1:len-offset
            vet(i) = (temp(i+1)-temp(i))/(x(i+offset)-x(i));
        end
        res(offset) = temp(1);
        temp = vet;
        offset = offset + 1;
    end
end

%usa una matrice di dimensione size(x)
function [res] = mn_dirdiff_matr(x,y)
    assert(length(x) == length(y));
    len = size(x,2);
    vet = zeros(len);
    for i=1:1:len
        vet(i,1) = y(i);
    end
    for j=2:1:len
        for i=1:1:len-1
            if (i <= len-j+1)
                vet(i,j) = (vet(i+1,j-1)-vet(i,j-1))/(x(i+j-1)-x(i));
            end
        end
    end
    res = vet(1,:);
end


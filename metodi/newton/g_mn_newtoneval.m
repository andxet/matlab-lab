function p = mn_newtoneval(c,xnodi,xx)
    %c = differenze divise
    %x = valori delle x su cui e' stato calcolato c
    %xx= punti su cui valutare
    %yy= valore del polinomio nel punto yy
    
    N = length(c);
    p = c(N);
    for k = N-1:-1:1
        p = p.*(xx-xnodi(k))+c(k);
    end
end

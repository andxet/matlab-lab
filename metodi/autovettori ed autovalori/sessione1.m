%-- 21/11/14 14:15 --%
format compact
a=randi(1000,10)
eig(a)
%ho trovato gli autovalori
[V,D]=eig(a)
%con il metodo delle potenze convergo verso il pi� grande o il pi� piccolo
%potenze(A, tolleranza u) => [l,v]
%Con potenze verifico 
[V,D]=eig(a)
[lexa,k]=max(abs(diag(D)))
vexa=V(:,k)
[l,v]=potenze (a,0.005)

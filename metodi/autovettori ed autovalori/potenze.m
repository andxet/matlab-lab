function [ l,v,L ] = potenze( A, toll, maxiter )%Max = max cicli, per evitare la divergenza del metodo, L opzionale=successione degli L calcolati
%potenze Utilizza il metodo delle potenze per trovare gli autovalori
%Per il debug:
%l deve approssimare max(abs(eig(a))) e
%v deve approssimare l'autovettore corrispondente
%[V,D]=eig(a);
%[lexa, k] = max(abs(diag(D)));
%vexa=V(:,k)
assert(size(A,1)==size(A,2));
N=length(A);

it=1; %Contatore di iterazioni

if nargin < 3
    maxiter=100;
end
%Vettore che contiene la successione degli l
L=zeros(maxiter);
%Vettore casuale
y=randn(N,1);
%Inizializzo l'errore a toll+1 cos� sono sicuro che sia > di toll
err=toll+1;
while(err>toll && it<maxiter)
    x=y/norm(y);
    y=A*x; % y->autovettore di lambda_max
    l=y'*x; % l->lambda_max
    L(it)=l;
    if it>1
        err=abs(l-L(it-1));
    end
    it=it+1;
end
v=y/norm(y);
disp('Iterazioni:')
disp(it)
end


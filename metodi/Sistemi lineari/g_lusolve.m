function x = lusolve(b, B, p)
%risolve il sistema lineare con termine noto B
% (B,p) sono l'output di ludecomp
%ovvero U=triu(B(p,:)) e 
%       L = eye(N)+tril(B(p,:),-1)
% per risolvere A*x=b, risolviamo L*U*x=b(p,:)
% risolverndo prima il sistema tr. inferiore L*y=b(p,:)
% col metodo di sostituzione in avanti 
% e poi quello tr. sup U*x=y col metodo di sostituzione all'indietro

%NOTA: non occorre formare esplicitamente le matrici L,U
%per il debug:
% i comandi [B,p]= ludecomp(A); x=lusolve(b,B,p)
%dovrebbero fornire lo stesso risultato di x=A\b

N = length(B);
if nargin < 3
    p = 1:N; %assume che non sia stato fatto pivoting
    %per tenere conto del pivoting successivamente, ogni volta che ho 
    %un indirizzamento per riga al posto di usare i uso p(i)
    %faccio il reindirizzamento solo su B e b ma non su 
    %x e y che rappresentano l'ordine delle variabili e quindi non va
    %mutato
end


% prima sostituzione
%potrei lavorare direttamente su x o addirittura direttamente su b :D :D :D
y = b;
for j =2:N
    y(j) = b(j) - B(j,1:j-1)*y(1:j-1);
end

% seconda sostituzione (all'indietro)
x = y;
for j = N:-1:1
    x(j) = (y(j) - B(j,j+1:N)*x(j+1:N))/B(j,j);
end

end


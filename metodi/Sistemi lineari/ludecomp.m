function A=ludecomp(A)
% Input: matrice A da decomporre
% Ouput: decomposizione A=LU in forma compatta
%  la U � salvata nella parte triangolare superiore della A
%  mentre tril(A) contiene le informazioni 
%  ovvero L=eye(N)+tril(A, -1);
% PER TESTARE:
% U=triu(A); L=eye(N)+tril(A,-1);
% deve essere A = L*U
% eye() = Matrice identit�
old = A;
N = length(A);

%p=1;%numero del passo
for p=1:(N-1)
    K=(p+1):N;
    %Calcolo il moltiplicatore k-esimo
    A(K,p) = A(K,p)/A(p,p);

    %Trasformo la riga k-esima
    A(K,K)=A(K,K)-A(K,p)*A(p,K);
end

U=triu(A);
L=eye(length(A))+tril(A,-1);
if old == L*U;
    disp('OK')
else
    disp('ERRORRRRRRRORORORROROROROROE!�"!"�$!"�$!"�$!"�$!�"$!�"')
end

end
function A=ludecomp(A)
% Input: matrice A da decomporre
% Output: decomposizione A=LU in forma conpatta
% mentre tril(A) contiene le informazioni per ricostruire la L
% ovvero L=eye(N)+tril(A)

%per testare:
%U=tril(A); L=eye(N)+tril(A,-1);
%deve essere A = L*U
    oldA = A;
    N=length(A); %assume che sia quadrata...

    for p=1:N-1
        K= p+1:N;
        A(K,p) = A(K,p)./A(p,p);
        A(K,K) = A(K,K) - A(K,p)*A(p,K);
    end
    
    U=triu(A);
    L=eye(N)+tril(A,-1);
    newA= L*U;    
    if (oldA == newA)
        disp('ok - decomposizione corretta')
    else
        disp('errore')
    end
end
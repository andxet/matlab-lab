function [A,PERM]=ludecomp2(A)
% Input: matrice A da decomporre
% Ouput: decomposizione A=LU in forma compatta
%  la U � salvata nella parte triangolare superiore della A
%  mentre tril(A) contiene le informazioni 
%  ovvero L=eye(N)+tril(A, -1);
old = A;
N = length(A);
PERM=[1:N];

for p=1:(N-1)
    A(PERM,:) % stampo la matrice riordinata, per debugging 
    % Scelgo elemento pivot
    [pivot, ind] = max(abs(A(PERM(p:N),p)));
    ind=ind(1)+p-1;
    pivot=A(ind,p);
    % Scambio gli elem del vettore perm
    PERM([ind,p])=PERM([p,ind]);
    % Debugging: non mettete il ; qui
    K=(p+1):N;
    % procedo nel calcolo, accedendo agli elementi trasformando ???
    % di riga (solo quelli!!) mediante il vettore di indici PERM
    %Calcolo il moltiplicatore k-esimo
    A(PERM(K),p) = A(PERM(K),p)/pivot;

    %Trasformo la riga k-esima
    A(PERM(K),K)=A(PERM(K),K)-A(PERM(K),p)*A(PERM(p),K);
    %Moltipliche
    %Calcolo la U
    disp('end\n')
end

U=triu(A);
L=eye(length(A))+tril(A,-1);
old=L*U;
if lu(old) == A;
    disp('OK')
else
    disp('ERRORRRRRRRORORORROROROROROE!�"!"�$!"�$!"�$!"�$!�"$!�"')
end
end
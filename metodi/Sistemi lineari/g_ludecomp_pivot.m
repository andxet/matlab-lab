function [A,PERM] = ludecomp_pivot(A)
    % Input: matrice A da decomporre
    % Output: decomposizione A=LU in forma conpatta
    % mentre tril(A) contiene le informazioni per ricostruire la L
    % ovvero L=eye(N)+tril(A)

    %per testare:
    %U=tril(A); L=eye(N)+tril(A,-1);
    %deve essere A = L*U
    oldA = A;
    N=length(A); %assume che sia quadrata...
    PERM = 1:N;
	for p=1:N-1
        %cerco il pivot
        [pivot,ind] = max(abs(A(p:N,p)));
        ind = ind+p-1;
        pivot = A(ind,p); %potrebbe essere negativo
        %scambio le righe
        PERM([p ind]) = PERM([ind p]);
        %effettuo il calcolo
        K =p+1:N;
        
        A(PERM(K),p) = A(PERM(K),p) / pivot;
        A(PERM(K),K) = A(PERM(K),K) - A(PERM(K),p) * A(PERM(p),K);
    end
    
    %CHECK
    Ap = A(PERM(:),:);
    U=triu(Ap);
    L=eye(N)+tril(Ap,-1);
    newA= L*U;
    oldA = oldA(PERM(:),:);
    
    if (oldA == newA)
        disp('ok - decomposizione corretta')
    else
        disp('errore')
        oldA
        newA
    end
%     test di funzionamento
%     U=triu(A);
%     L=eye(N)+tril(A,-1);
%     newA= L*U;    
%     if (oldA == newA)
%         disp('ok - decomposizione corretta')
%     else
%         disp('errore')
%     end
end
function Q=cavSimp(f, a, b)
%Function handle,
%Limite inferiore double,
%Limite superiore double
    Q = (b-a)*(1/6*f(a)+2/3*f((b+a)/2)+1/6*f(b));
    %Q = (b-a)*(f(a)+4*f((a+b)/2)+f(b))/6; %Versione ottimizzata del prof
end
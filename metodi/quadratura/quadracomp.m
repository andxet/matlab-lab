%Q.xnodi=[0,0.5,1];
%Q.pesi=[1/6,2/3,1/6];
%Q.A=0;
%Q.B=1;

function Q=quadracomp(f, a, b, N, Q)
% Approssima l'integrale di f(x) su [a,b] usando la quadratura numerica Q
% con l'intervallo
% f: function handle
% a,b: estremi di integrazione
% Q: struct che definisce la formula di quadratura 

if nargin<5
    %Default per la formula di quadratura
    Q.xnodi=0.5;
    Q.pesi=1;
    Q.A=0;
    Q.B=1;
end
if nargin<4
    N=1;
end

h=(b-a)/N;
ris(N)=0;
punti = a + h/(Q.B-Q.A) * (Q.xnodi-Q.A);%Del prof
for i=1:N
    xn= (i-1)*h+punti;
    fn = f(xn);
    ris(i)= h * Q.pesi * fn';
end
Q.ris=sum(ris)
end
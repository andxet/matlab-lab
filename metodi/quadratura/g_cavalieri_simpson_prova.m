H=2.^(-(0:8));
f = @(x) exp(x);
for k=1:length(H)
    Q = cavalieri_simpson(f,0,H(k));
    esatto = exp(H(k))-1;
    ERR(k) = Q -esatto;
end
loglog(H,ERR,'.-');
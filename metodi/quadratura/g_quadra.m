%function quadra(f,a,b,Q)
%f = funzione
%a = 
%b = 
%xnodi = posizione dei nodi   |
%w = pesi dei nodi            | -> definisco in una struct
%A,B = estremi dell'intervallo|
%       di riferimento      

%struct =   Q.xnodi= [0,1/2,1]
%           Q.pesi=  [1/6 ,2/3, 1/6]
%           Q.A=     0
%           Q.B=     1

function [Q] = quadra(f,a,b,Q)   
    xn = a+Q.xnodi*(b-a); %nodi in [a,b]
    fn = f(xn); %valori di f(x) nei nodi
    Q.ris = (b-a)*sum(Q.pesi.*fn); %calcolo del risultato della quadratura
end

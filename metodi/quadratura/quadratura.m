%Q.xnodi=[0,0.5,1];
%Q.pesi=[1/6,2/3,1/6];
%Q.A=0;
%Q.B=1;

function Q=quadratura(f, a, b, Q)
% Approssima l'integrale di f(x) su [a,b] usando la quadratura numerica Q
% con l'intervallo
% f: function handle
% a,b: estremi di integrazione
% Q: struct che definisce la formula di quadratura 

if nargin<4
    %Default per la formula di quadratura
    Q.xnodi=0.5;
    Q.pesi=1;
    Q.A=0;
    Q.B=1;
end

h=b-a;
%tox=@(t) a+t*(b+a);%mio, sbagliato
%xn = tox(Q.xnodi) %nodi in [a,b]
%fn = f(xn);
%Q.ris = h*sum(Q.pesi.*fn);

xn= a + h/(Q.B-Q.A) * (Q.xnodi-Q.A);%Del prof
fn = f(xn);
Q.ris= h * Q.pesi * fn';

end
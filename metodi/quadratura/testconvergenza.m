%Lunghezza intervallo decrescente
clear H
clear ERR
%H=2.^(-(0:15));
H=2.^(-(0:8));
f=@(x) exp(x)
for k=1:length(H)
    Q = cavSimp(f,0,H(k));
    esatto=exp(H(k))-1;
    ERR(k) = Q - esatto;
end
loglog(H,ERR,'.-')
grid on
xlabel('b-a')
ylabel('errore')
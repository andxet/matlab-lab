% function handle: f = @(x) definizione della function: es. x.*sin(x)
% cavalieri simpson: I = (b-a)* 1/6 f(a) + 4/6 f(m) + 1/6 f(b)

function [I] = cavalieri_simpson(f,a,b)
    I = (b-a)*(f(a) + 4*f((b+a)/2) + f(b))/6;
end
function a=dirdiff(x,y)
    assert(length(x) == length(y));
    K=length(x);
    F=zeros(K);
    F(:,1)=y;
    
    for I=2:K
        J=1:K-I+1
        F(J,I)=(F(J+1, I+1)-F(J,I-1))./(x(J+I-1)-x(J));
    end
    